<?php

namespace RussianSystems\Controller;


use RussianSystems\Response;

abstract class BaseController
{
    public function action()
    {
        return $this->getResponse();
    }

    /**
     * @return Response
     */
    abstract protected function getResponse();
}
