<?php


namespace RussianSystems\Controller;


use RussianSystems\Repository\CommentRepository;
use RussianSystems\Response;
use RussianSystems\TwigLoader;

class SaveCommentController extends BaseController
{

    /**
     * @return Response
     */
    protected function getResponse()
    {
        $parentId = $_POST['parent_id'];
        $body = $_POST['body'];
        if (empty($parentId) || empty($body)) {
            $response = new Response('404.twig');
            $response->setCode(404);
            return $response;
        }

        $message = '';
        try {
            $comment = CommentRepository::getInstance()->getComment($parentId);
            if (null === $comment) {
                throw new \InvalidArgumentException("The comment {$parentId} doesn't exist");
            }
            CommentRepository::getInstance()->saveComment($comment, $_POST['body']);

            $message = 'Comment has been added successfully';
        } catch (\Exception $e) {
            $message = 'Comment saving has been failed: ' . $e->getMessage();
        }

        $parentComment = CommentRepository::getInstance()->getComment($parentId);
        return new Response(null, [
            'message' => $message,
            'commentsTree' => TwigLoader::getInstance()->getTwig()->render('card.twig', ['card' => $parentComment])
        ]);
    }
}
