<?php


namespace RussianSystems\Controller;


use RussianSystems\Repository\CommentRepository;
use RussianSystems\Response;

class CommentListController extends BaseController
{

    /**
     * @return Response
     */
    protected function getResponse()
    {
        $topicId = $_GET['topic_id'];
        if (is_null($topicId)) {
            $response = new Response('404.twig');
            $response->setCode(404);
            return $response;
        }

        return new Response('comments.twig', ['comments' => CommentRepository::getInstance()->getCommentsByTopic($topicId)]);
    }
}
