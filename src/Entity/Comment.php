<?php

namespace RussianSystems\Entity;

class Comment
{
    private $id;
    private $parentId;
    private $body;
    private $topicId;
    private $createdAt;

    public static function create(array $row): self
    {
        $comment = new Comment();
        return $comment->setId($row['id'])
            ->setBody($row['body'])
            ->setParentId($row['parent_id'])
            ->setTopicId($row['topic_id'])
            ->setCreatedAt($row['created_at']);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Comment
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param mixed $parentId
     * @return Comment
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     * @return Comment
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     * @return Comment
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }
    private $children;

    /**
     * @return mixed
     */
    public function getTopicId()
    {
        return $this->topicId;
    }

    /**
     * @param mixed $topicId
     * @return Comment
     */
    public function setTopicId($topicId): self
    {
        $this->topicId = $topicId;

        return $this;
    }
}
