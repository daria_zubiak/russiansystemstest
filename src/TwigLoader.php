<?php


namespace RussianSystems;


use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigLoader
{
    private static $instance;

    private $twig;

    private function __construct()
    {
        $loader = new FilesystemLoader('templates');
        $this->twig = new Environment(
            $loader, [
                'cache' => 'cache',
            ]
        );
    }

    public static function getInstance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }
}
