<?php

namespace RussianSystems;

use RussianSystems\Controller\BaseController;
use RussianSystems\Controller\CommentListController;
use RussianSystems\Controller\SaveCommentController;

class Routing
{
    private static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @param $uri
     * @return BaseController
     */
    public function getController(string $uri): BaseController
    {
        switch ($uri) {
            case '/comment/save':
                return new SaveCommentController();
        }
        return new CommentListController();
    }
}
