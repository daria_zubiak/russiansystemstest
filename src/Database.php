<?php


namespace RussianSystems;


use PDO;
use PDOException;
use RuntimeException;

class Database
{
    private static $instance;

    private $pdo;

    public static function getInstance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public function getConnect()
    {
        $servername = "localhost";
        $username = "darya";
        $password = "darya";
        $database = 'russiansystems';

        if (null === $this->pdo) {
            try {
                $this->pdo = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                throw new RuntimeException("The user {$username} can't be logged to database {$database}:" . $e->getMessage());
            }
        }

        return $this->pdo;
    }

    public function __destruct()
    {
        $this->pdo = null;
    }
}
