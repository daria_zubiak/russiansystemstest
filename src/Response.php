<?php


namespace RussianSystems;


class Response
{
    public function __construct($template = null, array $parameters = [])
    {
        $this->template = $template;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $template
     * @return Response
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return Response
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return Response
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }
    /**
     * @var string
     */
    private $template;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var int
     */
    private $code = 200;

    public function render(): string
    {
        if (null === $this->getTemplate()) {
            return json_encode($this->getParameters());
        }

        return TwigLoader::getInstance()->getTwig()->render($this->getTemplate(), $this->getParameters());
    }

}
