<?php


namespace RussianSystems\Repository;

use PDO;
use RussianSystems\Database;
use RussianSystems\Entity\Comment;

class CommentRepository
{
    private static $instance;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getCommentsByTopic(int $topicId): array
    {
        $sth = Database::getInstance()->getConnect()->prepare(
            'SELECT * FROM comments WHERE parent_id IS NULL and topic_id=:topic_id',
            array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
        );
        $sth->execute(array(':topic_id' => $topicId));

        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);

        $comments = [];
        foreach ($rows as $row) {
            $comment = Comment::create($row);
            $comment->setChildren($this->getChildComments($comment));
            $comments[] = $comment;
        }

        return $comments;
    }

    public function getChildComments(Comment $parentComment): array
    {
        $sth = Database::getInstance()->getConnect()->prepare(
            'SELECT * FROM comments WHERE parent_id=:parent_id ORDER BY created_at ASC',
            array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
        );
        $sth->execute(array(':parent_id' => $parentComment->getId()));

        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);

        $comments = [];
        foreach ($rows as $row) {
            $comment = Comment::create($row);
            $comment->setChildren($this->getChildComments($comment));
            $comments[] = $comment;
        }

        return $comments;
    }

    public function getComment($id): ?Comment
    {
        $sth = Database::getInstance()->getConnect()->prepare(
            'SELECT * FROM comments WHERE id=:id',
            array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
        );
        $sth->execute(array(':id' => $id));

        $comments = $sth->fetchAll(PDO::FETCH_ASSOC);

        $comment = null;
        if ($comments) {
            $comment = Comment::create($comments[0]);
            $comment->setChildren($this->getChildComments($comment));
        }

        return $comment;
    }

    /**
     * @param Comment $comment
     * @param string $body
     */
    public function saveComment(Comment $comment, string $body)
    {
        $sth = Database::getInstance()->getConnect()->prepare(
            'INSERT INTO comments (parent_id, body, topic_id) VALUES (:parent_id, :body, :topic_id)',
            array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
        );

        $sth->execute(array(':parent_id' => $comment->getId(), ':body' => $body, ':topic_id' => $comment->getTopicId()));
    }
}
