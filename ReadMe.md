=========
## Server start

php7.2 -S localhost:8000
http://localhost:8000/?topic_id=1

## Database script

Database login/password in src/Database.php must be replaced by config file

```sql
CREATE DATABASE russiansystems;

create table topics
(
    id         int unsigned auto_increment
        primary key,
    title      varchar(100)                        null,
    created_at timestamp default CURRENT_TIMESTAMP not null
);

create table if not exists comments
(
	id int unsigned auto_increment
		primary key,
	parent_id int unsigned null,
	topic_id int unsigned not null,
	body varchar(100) null,
	created_at timestamp default CURRENT_TIMESTAMP not null,
	constraint comments_ibfk_1
		foreign key (topic_id) references topics (id),
	constraint comments_ibfk_2
		foreign key (parent_id) references comments (id)
);

INSERT INTO topics (title) VALUES ('programming'), ('yoga');
INSERT INTO comments (topic_id, body) VALUES (1, 'programming is my profession'), (2, 'yoga is my hobby');
```
