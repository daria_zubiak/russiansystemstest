class ReplyForm {
    constructor() {
        this.parentComentId = 0;

        let that = this;

        $(document).on('submit', '#js-save-comment-form', function (e) {
            e.preventDefault();
            that.saveComment($(this));
        });

        $(document).on('click', '.js-button-reply', function (e) {
            e.preventDefault();

            that.onButtonClick($(this));
        });
    }

    onButtonClick(button) {
        this.parentComentId = $(button.parents('.row')).find('input.js-card-id').val();
    }

    saveComment(form) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "/comment/save",
            data: {
                body: form.find('textarea').val(),
                parent_id: this.parentComentId
            },
            success: (data)=>{

                if (typeof data.message === 'string') {
                    alert(data.message);
                }

                $(form.parents('.modal')).modal('hide');

                if (typeof data.commentsTree === 'string') {
                    $('#card' + this.parentComentId).replaceWith(data.commentsTree);
                }
            },
            error: (data)=>{
                if (typeof data.message === 'string') {
                    alert(data.message);
                }
            }
        });
    }
}

new ReplyForm();
