<?php

require "vendor/autoload.php";

$controller = RussianSystems\Routing::getInstance()->getController($_SERVER['REQUEST_URI']);
$response = $controller->action();

echo $response->render();


